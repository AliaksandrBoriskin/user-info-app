JET.init({ ID: 'User Info App' });
JET.onLoad(function () {
    let response_channel = '/my_response_channel';
    document.getElementById("run").addEventListener("click", function () {
        JET.subscribe(response_channel, function (data) {
            document.getElementById('output').innerHTML = data;
        });

        JET.publish('/my_sample_channel', { 
            action: 'getActiveCell', responseChannel: response_channel 
        });
    });
});